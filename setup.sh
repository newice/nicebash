#! /bin/bash
if (( $# != 1)); then
	echo "Usage: ./setup.sh (<user> | all)"
	exit 1
fi 
destPath="/etc/profile"
#destPath="./profile"

if [[ $1 != "all" ]]; then
	destPath="/home/$1/.bashrc"
fi
nice="nicebash.rc"
addline=". $(pwd)/$nice"
		
if (( $(grep "$addline" $destPath | wc -l) == 0 )); then
	echo $addline >> $destPath
	
else
	echo "file $destPath already modified. Nothing to do."
fi
exit 0